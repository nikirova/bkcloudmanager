
# bkCloudManager

It's a background application that supports two fold communication:
-	Connects to Azure IoT Hub to send telemetry data to and receive commands from IoT Suite
-	Connects to bkIPC_AppService to send commands to and receive telemetry data from bkNstApplication 

> During operation, Gateway.db (stored in c:\Database folder) is read to obtain 
> neccessary information such as Azure IoT connection strings

## Hardware requirements

** Raspberry Pi 3, Dragonboard 410c **


## Operating system requirements

** Windows 10 IoT Core ** 


## Nuget Package requirements

-	Microsoft.Azure.Devices.Client ver 1.4.2
-	Microsoft.Data.Sqlite ver 1.1.1
-	Microsoft.NETCore.UniversalWindowsPlatform ver 5.4.0
-	WindowsAzure.Storage ver 8.4.0

## Build the sample

1. Start Microsoft Visual Studio 2017 and select **File** \> **Open** \> **Project/Solution**.
2. Starting in the "Middleware" folder, go to the bkCloudManager subfolder. Double-click the Visual Studio 2017 Solution (.sln) file.
3. In solution configuration, select "Debug", "ARM", "Remote Machine" and press "Find" to locate the connected device. 
4. Press Ctrl+Shift+B, or select **Build** \> **Build Solution**.


## Licensing

"The code in this project is licensed under NST license."
