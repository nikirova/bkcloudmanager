﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Azure.Devices.Client;
using Windows.Foundation.Collections;
using comNST;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Windows.Storage;
using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Microsoft.Azure.Devices.Shared;
using Windows.System.Threading;
using Windows.System;
using Windows.UI.Xaml;
using static comNST.CloudMessage;

namespace BKCloudManager

{

    class IoTClient
    {

        public static DeviceClient deviceClient;
        private static int NumOfRetries = 0;
        static ConnectionStatus currentConnectionStatus;
        private static string gatewayName = null;
        public async static Task Start()
        {
            try
            {
                // Get from database the name of the Gateway
                gatewayName = await dbGetGatewayName();

                // Get from the Database the Azure credentials for Gateway
                string GatewayConnectionString = await dbGetGatewayAzureConnectionString();
                deviceClient = DeviceClient.CreateFromConnectionString(GatewayConnectionString, TransportType.Mqtt);

                await Logging.WriteDebugLog("IoT Hub connected: {0}", GatewayConnectionString);

                deviceClient.SetConnectionStatusChangesHandler(ConnectionStatusChanged);


                // Set a handler for automatically inform that a change to Gateway desired properties occured 
                await deviceClient.SetDesiredPropertyUpdateCallbackAsync(OnDesiredPropertyChanged, null);

                // for handling METHODS refer to "CloudRegistration" Project

                // JUST CHECK WHETHER THERE IS CONNECTION WITH CLOUD
                try
                {
                    Message xx = await deviceClient.ReceiveAsync();
                    if (xx != null)
                    {
                        await Logging.WriteDebugLog("Start - A command received probably lost!!!!");
                        await deviceClient.CompleteAsync(xx);
                    }
                    currentConnectionStatus = ConnectionStatus.Connected;

                    NumOfRetries = 0;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Start - Testing Connection:" + ex.Message);
                    await Logging.WriteSystemLog("Start - Testing Connection: {0}", ex.Message);
                    // 3-11-2017 Comments inserted
                    //if (ex.Message.Equals("Transient error occured, please retry.")) 
                    //{ 
                        NumOfRetries++;
                    //}

                }

                await ReceiveCloudCommand();

            }
            catch (System.Runtime.InteropServices.COMException comExc)
            {
                Debug.WriteLine("Start: Exception thrown: " + comExc.Message);
                // string exMessage = "Start: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("Start - COMException - {0}", comExc.Message);
                NumOfRetries++; // 3-11-2017 inserted
                Task RetryTask = Task.Run(RetryConnection); // 3-11-2017 inserted
                //Application.Current.Exit();

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Start: Exception thrown: " + ex.Message);
                // string exMessage = "Start: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("Start - General exception - {0}", ex.Message);
                NumOfRetries++; // 3-11-2017 inserted
                Task RetryTask = Task.Run(RetryConnection); // 3-11-2017 inserted
            }

        }

        private static async Task OnDesiredPropertyChanged(TwinCollection desiredProperties, object userContext)
        {
            try
            {
                Newtonsoft.Json.Linq.JObject d = Newtonsoft.Json.Linq.JObject.FromObject(desiredProperties["DeleteCommandHistory"]);
                var properties = d.Properties();
                foreach (var x in properties)
                {
                    // Get messageId than needs to be deleted
                    string messageId = x.Name;

                    // Delete messageId from reported properties
                    TwinCollection nstCommandHistory, reportedProperties;
                    reportedProperties = new TwinCollection();
                    nstCommandHistory = new TwinCollection();
                    nstCommandHistory[messageId] = null;
                    reportedProperties["NstCommandHistory"] = nstCommandHistory;
                    await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);
                    
                    await Logging.WriteDebugLog("IoT Suite: Delete command with message id: ", messageId);
                }
                
            }
            catch (Exception ex)
            {

                Debug.WriteLine("OnDesiredPropertyChanged: ", ex.Message);
                await Logging.WriteSystemLog("OnDesiredPropertyChanged - {0}", ex.Message);
            }
        }

        // refer to https://docs.microsoft.com/sv-se/dotnet/api/microsoft.azure.devices.client.connectionstatus?view=azuredevicesclient-1.3.0 
        private static void ConnectionStatusChanged(ConnectionStatus status, ConnectionStatusChangeReason reason)
        {
            string strReason = clarifyConnectionStatusChangeReason(reason);
            switch (status)
            {
                case ConnectionStatus.Disabled:
                    Debug.WriteLine("Connection Status Changed to DISABLED due to  " + strReason);
                    Logging.WriteDebugLog("Connection Status Changed to DISABLED due to {0}", strReason);
                    break;
                case ConnectionStatus.Disconnected:
                    Debug.WriteLine("Connection Status Changed to DISCONNECTED due to  " + strReason);
                    currentConnectionStatus = ConnectionStatus.Disconnected;
                    Task RetryTask = Task.Run(RetryConnection);
                    //await RetryConnection();
                    Logging.WriteDebugLog("Connection Status Changed to DISCONNECTED due to {0}", strReason);
                    break;
                case ConnectionStatus.Connected:

                    currentConnectionStatus = ConnectionStatus.Connected;
                    Debug.WriteLine("Connection Status Changed to CONNECTED due to  " + strReason);
                    Logging.WriteDebugLog("Connection Status Changed to CONNECTED due to {0}", strReason);
                    break;
                case ConnectionStatus.Disconnected_Retrying:
                    currentConnectionStatus = ConnectionStatus.Disconnected;
                    Debug.WriteLine("Connection Status Changed to DISCONNECTED_RETRYING due to  " + strReason);
                    Logging.WriteDebugLog("Connection Status Changed to DISCONNECTED_RETRYING due to {0}", strReason);
                    break;

            }

        }

        private static async Task RetryConnection()
        {

            double x = computeDelay(NumOfRetries);
            // SEND message to NST Administrator Host
            Debug.WriteLine("Retry No: " + NumOfRetries + " - Delay (min): " + x);
            await Logging.WriteDebugLog("Retry No: - {0} with Delay {1}", NumOfRetries.ToString(), x.ToString());
            // in a background process replace it with a timer

            await Task.Delay(TimeSpan.FromMinutes(x));

            Debug.WriteLine("Try to Re-Connect");
            await Logging.WriteDebugLog("Try to Re-Connect");
            //NumOfRetries++;
            deviceClient.Dispose();
            await Start();

        }

        private static double computeDelay(int x)
        {
            double y;

            if ((x == 0) || (x == 1) || (x == 2))
                return 0.5; // Correct the value

            y = 0.5; // Correct the value

            // f(x) = (ln((1 + 50 * x)) ^ 2.34) / (2.29 + 1.0045 ^ (-0.19 * x ^ 2.7))

            //y = Math.Round((Math.Pow(Math.Log10(1+50*x),2.34)) / (2.29 + Math.Pow(1.0045, (-0.19 * Math.Pow(x, 2.7)))),2);
            //Debug.WriteLine("X = " + x + " Y = " +y);

            return y;
        }

        public static async Task SendDataToCloud(string data)
        {
            try
            {

                NstMessage dataReceived = new NstMessage(data);

                Message eventMessage = new Message(Encoding.UTF8.GetBytes(data));

                switch (dataReceived.Response)
                {
                    case NstMessage.TELE_RESPONSE:

                        //await deviceClient.SendEventAsync(eventMessage);
                        bool okResult = await sendTelemetryDataToCloud(dataReceived.Ipnode, dataReceived.Resource, dataReceived.Value);

                        if( okResult == true)
                        {
                            await Logging.WriteDebugLog("Data sent to Cloud: {0}", data);
                        }
                        else
                        {
                            await Logging.WriteDebugLog("Due to lost connection data DID not sent to Cloud: {0}", data);
                        }
                        break;

                    case NstMessage.ALERT_RESPONSE: // Responce Code: Currently 98,  At the next release 05 (WRONG DATA MEASUREMENT)
                    case NstMessage.HBE_ALERT_RESPONSE: // Responce Code: 97 (CMG DEAD)

                        //await deviceClient.SendEventAsync(eventMessage);
                        await Logging.WriteDebugLog("Data sent to Cloud: {0}", data);

                        okResult = await sendAlertDataToCloud(dataReceived);

                        if (okResult == true)
                        {
                            await Logging.WriteDebugLog("Alert sent to Cloud: {0}", data);
                        }
                        else
                        {
                            await Logging.WriteDebugLog("Due to lost connection Alert DID not sent to Cloud: {0}", data);
                        }
                        break;

                    default:
                        // send data to cloud as it is

                        // AT PRODUCTION: TO BE DELETED 
                        await deviceClient.SendEventAsync(eventMessage);

                        bool updateTwin = true;

                        // update twin properties
                        TwinCollection nstCommandHistory, responceReceived, reportedProperties;
                        reportedProperties = new TwinCollection();

                        nstCommandHistory = new TwinCollection();
                        responceReceived = new TwinCollection();
                        string name = await dbGetCMDUniqueID(dataReceived.Ipnode);
                        responceReceived["Name"] = name;
                        string msgid = await dbGetAzureMessageID(dataReceived.Token);
                        responceReceived["MessageId"] = msgid;

                        // Get from the database the command that had invoked the current responce
                        string command = await dbGetCommandName(dataReceived.Token);
                        switch (dataReceived.Response)
                        {
                            case NstMessage.OK_RESPONSE:
                                
                                if (command.Equals(NstMessage.GET_COMMAND))
                                {
                                    responceReceived["Result"] = dataReceived.Value;
                                }
                                else if (command.Equals(NstMessage.PNG_COMMAND))
                                {
                                    responceReceived["Result"] = "Device is Alive";
                                }
                                else if(command.Equals(NstMessage.SUB_COMMAND))
                                {
                                    // It is the first responce to a SUB command
                                    updateTwin = false;
                                    // Send it as telemetry data
                                    okResult = await sendTelemetryDataToCloud(dataReceived.Ipnode, dataReceived.Resource, dataReceived.Value);

                                    if (okResult == true)
                                    {
                                        await Logging.WriteDebugLog("Data sent to Cloud: {0}", data);
                                    }
                                    else
                                    {
                                        await Logging.WriteDebugLog("Due to lost connection data DID not sent to Cloud: {0}", data);
                                    }
                                }
                                else {
                                    await Logging.WriteDebugLog("OK result received to an Unknown command");
                                }
                                break;
                            case NstMessage.NOK_RESPONSE:
                                responceReceived["Result"] = "Not OK";
                                break;
                            case NstMessage.BUSY_RESPONSE:
                                responceReceived["Result"] = "Network is Busy";
                                break;
                            case NstMessage.RNF_RESPONSE:
                                responceReceived["Result"] = "Resource not found";
                                break;
                            case NstMessage.SNF_RESPONSE:
                                if (command.Equals(NstMessage.SUB_COMMAND))
                                {

                                    // It is the first responce to a SUB command
                                    updateTwin = false; // Do not update twin 
                                    // Send it as Alert data
                                    okResult = await sendAlertDataToCloud(dataReceived);

                                    if (okResult == true)
                                    {
                                        await Logging.WriteDebugLog("Alert sent to Cloud: {0}", data);
                                    }
                                    else
                                    {
                                        await Logging.WriteDebugLog("Due to lost connection Alert DID not sent to Cloud: {0}", data);
                                    }
                                }
                                else
                                {
                                    responceReceived["Result"] = "CMD not found";
                                }
                                break;
                            case NstMessage.WVDR_RESPONSE:
                                responceReceived["Result"] = "Wrong measurement";
                                break;
                        }
                        if (updateTwin == true)  // response to a command except SUB
                        {
                            nstCommandHistory[msgid] = responceReceived;
                            reportedProperties["NstCommandHistory"] = nstCommandHistory;

                            await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);

                            // Send a msg informing that a command responce is available at twin reported
                            // to be initiated in NEXT RELEASE
                            //await informIoTSuiteForCommandResponse(msgid);


                            // delete the processed command from app_commands
                            int result = await dbDeleteAppCommand(dataReceived.Token);
                            if (result != 1)
                            {
                                // TBD
                                // Handle Error Code
                            }

                            await Logging.WriteDebugLog("Data sent to Cloud: {0}", data);
                        }
                        break;
                }
                
                

            }
            catch (Exception ex)
            {
                await Logging.WriteSystemLog("SendDataToCloud - {0}", ex.Message);
            }

        }

        private static async Task<bool> informIoTSuiteForCommandResponse(string msgid)
        {
            bool result = false;
            try
            {
                if (currentConnectionStatus == ConnectionStatus.Connected)
                {
                    ResponseCommandData response = new ResponseCommandData();


                    response.GatewayName = gatewayName;
                    response.MessageId = msgid;
                    var msg = new Message(Encoding.UTF8.GetBytes(response.Stringify()));
                    await deviceClient.SendEventAsync(msg);
                 
                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                await Logging.WriteSystemLog("informIoTSuiteForCommandResponse - {0}", ex.Message);
                result = false;
            }
            return result;
        }



        private static async Task<bool> sendAlertDataToCloud(NstMessage dataReceived)
        {
            bool result = false;
            try
            {
                if (currentConnectionStatus == ConnectionStatus.Connected)
                {
                    AlertData alertData = new AlertData();
                    RaiseAlertData raiseAlertData = new RaiseAlertData();
                    switch (dataReceived.Response)
                    {
                        case NstMessage.ALERT_RESPONSE:
                            alertData.DeviceId = await dbGetCMDUniqueID(dataReceived.Ipnode);
                            alertData.Message = "Wrong Measurement from " + dataReceived.Resource +
                                                " resource of " + alertData.DeviceId + " CMD";
                            alertData.GatewayId = gatewayName;
                            alertData.ObjectType = "AlertInfo";
                            var msg = new Message(Encoding.UTF8.GetBytes(alertData.Stringify()));
                            await deviceClient.SendEventAsync(msg);

                            raiseAlertData.DeviceId = alertData.DeviceId;
                            raiseAlertData.Alert = 1;
                            var msgI = new Message(Encoding.UTF8.GetBytes(raiseAlertData.Stringify()));
                            await deviceClient.SendEventAsync(msgI);
                            break;

                        case NstMessage.HBE_ALERT_RESPONSE:
                            alertData.DeviceId = gatewayName;
                            alertData.Message = "CMG " + dataReceived.Ipnode +
                                                " is not responding";
                            alertData.GatewayId = alertData.DeviceId;
                            alertData.ObjectType = "AlertInfo";
                            msg = new Message(Encoding.UTF8.GetBytes(alertData.Stringify()));
                            await deviceClient.SendEventAsync(msg);

                            raiseAlertData.DeviceId = alertData.DeviceId;
                            raiseAlertData.Alert = 1;
                            msgI = new Message(Encoding.UTF8.GetBytes(raiseAlertData.Stringify()));
                            await deviceClient.SendEventAsync(msgI);
                            break;
                        case NstMessage.SNF_RESPONSE: 
                            // CMD not found responce to a SUB command
                            // It should be treated as an Alert to Cloud
                            alertData.DeviceId = await dbGetCMDUniqueID(dataReceived.Ipnode);
                            alertData.Message = "CMD " + alertData.DeviceId + " is not responding";
                            alertData.GatewayId = gatewayName;
                            alertData.ObjectType = "AlertInfo";
                            msg = new Message(Encoding.UTF8.GetBytes(alertData.Stringify()));
                            await deviceClient.SendEventAsync(msg);

                            raiseAlertData.DeviceId = alertData.DeviceId;
                            raiseAlertData.Alert = 1;
                            msgI = new Message(Encoding.UTF8.GetBytes(raiseAlertData.Stringify()));
                            await deviceClient.SendEventAsync(msgI);
                            break;

                    }
                    result = true;
                }
                else
                {
                    result = false;
                }


            }
            catch (Exception ex)
            {
                await Logging.WriteSystemLog("sendAlertDataToCloud - {0}", ex.Message);
                result = false;
            }
            return result;
        }


        private static async Task<bool> sendTelemetryDataToCloud(string cmdIpnode, string resource, double value)
        {
            bool result = false;
            try
            {
                if (currentConnectionStatus == ConnectionStatus.Connected)
                {
                    switch (resource)
                    {
                        case NstMessage.HUMMIDITY_AIR:
                            comNST.CloudMessage.AirHumidityData dataAH = new comNST.CloudMessage.AirHumidityData();
                            dataAH.DeviceId = await dbGetCMDUniqueID(cmdIpnode);
                            dataAH.AirHumidity = value;
                            var msg = new Message(Encoding.UTF8.GetBytes(dataAH.Stringify()));
                            //msg = new Message(Serialize(dataAH));
                            await deviceClient.SendEventAsync(msg);
                            break;

                        case NstMessage.HUMMIDITY_SOIL:
                            comNST.CloudMessage.SoilHumidityData dataSH = new comNST.CloudMessage.SoilHumidityData();
                            dataSH.DeviceId = await dbGetCMDUniqueID(cmdIpnode);
                            dataSH.SoilHumidity = value;
                            msg = new Message(Encoding.UTF8.GetBytes(dataSH.Stringify()));
                            //msg = new Message(Serialize(dataSH));
                            await deviceClient.SendEventAsync(msg);
                            break;
                        case NstMessage.TEMPERATURE_AIR:
                            comNST.CloudMessage.AirTemperatureData dataAT = new comNST.CloudMessage.AirTemperatureData();
                            dataAT.DeviceId = await dbGetCMDUniqueID(cmdIpnode);
                            dataAT.AirTemperature = value;
                            msg = new Message(Encoding.UTF8.GetBytes(dataAT.Stringify()));
                            //msg = new Message(Serialize(dataAT));
                            await deviceClient.SendEventAsync(msg);
                            break;
                        case NstMessage.TEMPERATURE_SOIL:
                            comNST.CloudMessage.SoilTemperatureData dataST = new comNST.CloudMessage.SoilTemperatureData();
                            dataST.DeviceId = await dbGetCMDUniqueID(cmdIpnode);
                            dataST.SoilTemperature = value;
                            msg = new Message(Encoding.UTF8.GetBytes(dataST.Stringify()));
                            //msg = new Message(Serialize(dataST));
                            await deviceClient.SendEventAsync(msg);
                            break;
                        case NstMessage.BATTERY_LEVEL:
                            comNST.CloudMessage.BatteryData dataBA = new comNST.CloudMessage.BatteryData();
                            dataBA.DeviceId = await dbGetCMDUniqueID(cmdIpnode);
                            dataBA.Battery = value;
                            msg = new Message(Encoding.UTF8.GetBytes(dataBA.Stringify()));
                            await deviceClient.SendEventAsync(msg);
                            break;
                    }
                    result = true;
                }
                else
                {
                    result = false;
                }

               
            }
            catch (Exception ex)
            {
                await Logging.WriteSystemLog("sendTelemetryDataToCloud - {0}", ex.Message);
                result = false;
            }
            return result;
        }


        private static async Task<string> dbGetCommandName(string token)
        {
            string connectDB = "Filename=c:\\Database\\Gateway.db";
            string selectStatement = "SELECT commandname FROM app_commands WHERE token = " + "\"" + token.ToUpper() + "\"";
            string cmdName = null;
            using (SqliteConnection db = new SqliteConnection(connectDB))
            {
                db.Open();
                using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                {
                    using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                    {
                        try
                        {
                            while (rdr.Read())
                            {
                                cmdName = rdr.GetString(0);
                                //Debug.WriteLine("Azure Connection String:" + azureID);
                            }
                            return cmdName;
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("dbGetCommandName:" + error.Message);
                            await Logging.WriteDebugLog("dbGetCommandName - {0}", error.Message);
                            db.Close();
                            return null;
                        }
                    }
                }
            }

        }


        private static async Task<string> dbGetCMDUniqueID(string cmdIpnode)
        {
            string connectDB = "Filename=c:\\Database\\Gateway.db";
            string selectStatement = "SELECT CMD_Unique_ID FROM CMDs WHERE CMD_IPV6_Address = " + "\"" + cmdIpnode + "\"";
            string cmdName = null;
            using (SqliteConnection db = new SqliteConnection(connectDB))
            {
                db.Open();
                using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                {
                    using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                    {
                        try
                        {
                            while (rdr.Read())
                            {
                                cmdName = rdr.GetString(0);
                                //Debug.WriteLine("Azure Connection String:" + azureID);
                            }
                            return cmdName;
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("dbGetCMDUniqueID:" + error.Message);
                            await Logging.WriteDebugLog("dbGetCMDUniqueID - {0}",  error.Message);
                            db.Close();
                            return null;
                        }
                    }
                }
            }

        }

        //private static byte[] Serialize(object obj)
        //{
        //    string json = JsonConvert.SerializeObject(obj);
        //    return Encoding.UTF8.GetBytes(json);

        //}




        private static async Task ReceiveCloudCommand()
        {
            //Debug.WriteLine("------------------------------------------------");
            //Debug.WriteLine("\nReceiving cloud to device messages from service");

            while (true)
            {

                try
                {
                    if (currentConnectionStatus == ConnectionStatus.Connected)
                    {
                        Message receivedCommand = await deviceClient.ReceiveAsync();
                        if (receivedCommand == null) continue;


                        // confirm to IoT Hub that command has received
                        await deviceClient.CompleteAsync(receivedCommand);


                        string deviceName = null;
                        string cmdName = null;

                        receivedCommand.Properties.TryGetValue("CommandName", out cmdName);
                        receivedCommand.Properties.TryGetValue("DeviceId", out deviceName);


                        switch (cmdName)
                        {
                            case "UpdateDatabase": // GATEWAY command
                                await Logging.WriteDebugLog("{0} - ReceiveCloudCommand - {1}", DateTime.Now.ToLocalTime().ToString(), cmdName);
                                bool result = await gtCommands.GetDBFileFromAzure();
                                // Update IoT Suite about the status of the command
                                if (result == true)
                                {
                                    await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "Database file downloaded successfully");
                                }
                                else
                                {
                                    await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "System error occurred. Refer to log file.");
                                }
                                break;
                            case "Reboot": // GATEWAY command
                                await Logging.WriteDebugLog("{0} - ReceiveCloudCommand - {1}", DateTime.Now.ToLocalTime().ToString(), cmdName);
                                await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "Gateway will reboot immediately.");
                                gtCommands.restartDragonBoard(null);

                                break;
                            case "UploadLogfile": // GATEWAY command
                                receivedCommand.Properties.TryGetValue("Application", out string application);
                                await Logging.WriteDebugLog("{0} - ReceiveCloudCommand - {1} of {2}", DateTime.Now.ToLocalTime().ToString(), cmdName, application);
                                result = await uploadLogFiles(application);
                                // Update IoT Suite about the status of the command
                                if (result == true)
                                {
                                    await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "Log files uploaded successfully");

                                }
                                else
                                {
                                    await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "System error occurred. Refer to log file.");
                                }
                                break;
                            default:
                                // Command to NST NETWORK
                                await sendCommandToNstNetwork(receivedCommand);

                                break;
                        }
                    }

                   
                }

                catch (Exception ex)
                {
                    Debug.WriteLine("ReceiveCloudCommand: Exception thrown: " + ex.Message);

                    await Logging.WriteSystemLog("ReceiveCloudCommand - {0}", ex.Message);

                }
            }

        }

        private async static Task sendCommandToNstNetwork(Message receivedCommand)
        {
            try
            {
                //read input and create JSON message
                NstMessage message = new NstMessage();

                // string commandName = Encoding.ASCII.GetString(receivedCommand.GetBytes());

                // NEXT RELEASE
                message.Appid = "2"; //set default value -- BOUTARIS 

                string deviceName = null;
                string cmgName = null;
                string resource = null;
                string cmdName = null;

                receivedCommand.Properties.TryGetValue("CommandName", out cmdName);
                receivedCommand.Properties.TryGetValue("DeviceId", out deviceName);
                receivedCommand.Properties.TryGetValue("CMGName", out cmgName);
                receivedCommand.Properties.TryGetValue("Resource", out resource);


                switch (cmdName)
                {
                    
                    case "GetValue":
                        message.Command = comNST.NstMessage.GET_COMMAND;
                        break;
                    case "PingDevice":
                        message.Command = comNST.NstMessage.PNG_COMMAND;
                        break;
                    case "ResetDevice":
                        await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "CMD will reboot immediately.");
                        message.Command = comNST.NstMessage.RST_COMMAND;
                        break;
                    case "SubscribeResource":
                        await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "Resource will be subscribed immediately. Telemetry data will start comming.");
                        message.Command = comNST.NstMessage.SUB_COMMAND;
                        break;
                    case "ResetCMG":
                        await updateTwinReportedProperties(deviceName, receivedCommand.MessageId, "CMG will reboot immediately.");
                        message.Command = comNST.NstMessage.RSG_COMMAND;
                        break;
                }


                switch (resource)
                {
                    case comNST.NstMessage.HUMMIDITY_AIR_NAME:
                        message.Resource = comNST.NstMessage.HUMMIDITY_AIR;
                        break;
                    case comNST.NstMessage.HUMMIDITY_SOIL_NAME:
                        message.Resource = comNST.NstMessage.HUMMIDITY_SOIL;
                        break;
                    case comNST.NstMessage.TEMPERATURE_AIR_NAME:
                        message.Resource = comNST.NstMessage.TEMPERATURE_AIR;
                        break;
                    case comNST.NstMessage.TEMPERATURE_SOIL_NAME:
                        message.Resource = comNST.NstMessage.TEMPERATURE_SOIL;
                        break;
                    case comNST.NstMessage.BATTERY_LEVEL_NAME:
                        message.Resource = comNST.NstMessage.BATTERY_LEVEL;
                        break;
                }

                if (!cmdName.Equals("ResetCMG"))
                {
                    message.Ipnode = await dbGetIpnode(deviceName, "CMD");
                }
                else
                {
                    message.Ipnode = await dbGetIpnode(cmgName, "CMG");
                }

                message.Azuremessageid = receivedCommand.MessageId;

                string jsonMessage = message.Stringify();
                //Debug.WriteLine("\t{0}> received Command: {1}", DateTime.Now.ToLocalTime(), jsonMessage);
                await Logging.WriteDebugLog("{0} - ReceiveCloudCommand - {1}", DateTime.Now.ToLocalTime().ToString(), jsonMessage);

                // Send Command to NstApplication
                await sendToNstApp(jsonMessage);
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendCommandToNstNetwork: Exception thrown: " + ex.Message);

                await Logging.WriteSystemLog("sendCommandToNstNetwork - {0}", ex.Message);
                
            }


        }
    

        private async static Task updateTwinReportedProperties(string deviceName, string msgId, string result)
        {
            try
            {
                // update twin properties
                TwinCollection nstCommandHistory, responceReceived, reportedProperties;
                reportedProperties = new TwinCollection();

                nstCommandHistory = new TwinCollection();
                responceReceived = new TwinCollection();
                responceReceived["Name"] = deviceName;
                responceReceived["MessageId"] = msgId;
                responceReceived["Result"] = result;
                nstCommandHistory[msgId] = responceReceived;
                reportedProperties["NstCommandHistory"] = nstCommandHistory;

                await deviceClient.UpdateReportedPropertiesAsync(reportedProperties);

                // To be intiated at NEXT RELEASE
                // await informIoTSuiteForCommandResponse(msgId);


            }
            catch (Exception ex)
            {
                Debug.WriteLine("updateTwinReportedProperties: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("updateTwinReportedProperties - {0}", ex.Message);
            }

        }


        
    private async static Task<bool> uploadLogFiles(string application)
        {
            StorageFile file;

            // copy log files to current directory
            DateTime dtNow = DateTime.Now;
            string currentDate = dtNow.ToString("yyyyMMdd");
            string srcDebugFileName = null;
            string srcDebugFileNameFull = null;
            string srcSystemFileName = null;
            string srcSystemFileNameFull = null;
            string desDebugFileNameFull = null;
            string desSystemFileNameFull = null;
            try
            {
                switch (application)
                {
                    case "PortManager":
                        srcDebugFileName = "bkPortManager_Debug_" + currentDate + ".Log";
                        srcDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\bkPortManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        desDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        srcSystemFileName = "bkPortManager_System_" + currentDate + ".Log";
                        srcSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\bkPortManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        desSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        break;
                    case "CloudManager":
                        srcDebugFileName = "BKCloudManager_Debug_" + currentDate + ".Log";

                        srcSystemFileName = "BKCloudManager_System_" + currentDate + ".Log";


                        //srcDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        //desDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        //srcSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        //desSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        break;
                    case "NstApplication":
                        srcDebugFileName = "bkNstApplication_Debug_" + currentDate + ".Log";
                        srcDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\bkNstApplication-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        desDebugFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcDebugFileName;
                        srcSystemFileName = "bkNstApplication_System_" + currentDate + ".Log";
                        srcSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\bkNstApplication-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        desSystemFileNameFull = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\" + srcSystemFileName;
                        break;
                    case "NstIPC":
                        break;
                }
                // Upload DEBUG log
                if (!application.Equals("CloudManager"))
                {

                    if (System.IO.File.Exists(srcDebugFileNameFull))
                    {

                        System.IO.File.Copy(srcDebugFileNameFull, desDebugFileNameFull, true);
                        file = await ApplicationData.Current.LocalFolder.GetFileAsync(srcDebugFileName);
                        using (Stream fileStream = await file.OpenStreamForReadAsync())
                        {
                            await deviceClient.UploadToBlobAsync(srcDebugFileName, fileStream);
                        }
                        System.IO.File.Delete(desDebugFileNameFull);
                        Debug.WriteLine("Log File: " + srcDebugFileName + " Successfully uploaded");
                        await Logging.WriteDebugLog("Log File: {0} successfully uploaded", srcDebugFileName);

                    }
                    else
                    {
                        await Logging.WriteDebugLog("File {0} not found. No upload done", srcDebugFileNameFull);
                    }

                    // Upload System log

                    if (System.IO.File.Exists(srcSystemFileNameFull))
                    {
                        System.IO.File.Copy(srcSystemFileNameFull, desSystemFileNameFull, true);
                        file = await ApplicationData.Current.LocalFolder.GetFileAsync(srcSystemFileName);
                        using (Stream fileStream = await file.OpenStreamForReadAsync())
                        {
                            await deviceClient.UploadToBlobAsync(srcSystemFileName, fileStream);
                        }
                        System.IO.File.Delete(desSystemFileNameFull);
                        Debug.WriteLine("Log File: " + srcSystemFileName + " Successfully uploaded");
                        await Logging.WriteDebugLog("Log File: {0} successfully uploaded", srcSystemFileName);
                    }
                    else
                    {
                        await Logging.WriteDebugLog("File {0} not found. No upload done", srcSystemFileNameFull);
                    }
                }
                else // CloudManager
                {
                    try
                    {
                        file = await ApplicationData.Current.LocalFolder.GetFileAsync(srcDebugFileName);
                        using (Stream fileStream = await file.OpenStreamForReadAsync())
                        {
                            await deviceClient.UploadToBlobAsync(srcDebugFileName, fileStream);
                        }

                        Debug.WriteLine("Log File: " + srcDebugFileName + " Successfully uploaded");
                        await Logging.WriteDebugLog("Log File: {0} successfully uploaded", srcDebugFileName);
                    }
                    catch (FileNotFoundException)
                    {
                        await Logging.WriteDebugLog("File {0} not found. No upload done", srcDebugFileName);
                    }


                    // Upload System log
                    try
                    {
                        file = await ApplicationData.Current.LocalFolder.GetFileAsync(srcSystemFileName);
                        using (Stream fileStream = await file.OpenStreamForReadAsync())
                        {
                            await deviceClient.UploadToBlobAsync(srcSystemFileName, fileStream);
                        }

                        Debug.WriteLine("Log File: " + srcSystemFileName + " Successfully uploaded");
                        await Logging.WriteDebugLog("Log File: {0} successfully uploaded", srcSystemFileName);
                    }
                    catch (FileNotFoundException)
                    {
                        await Logging.WriteDebugLog("File {0} not found. No upload done", srcSystemFileName);
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("uploadLogFiles: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("uploadLogFiles - {0}", ex.Message);

                return false;
            }
        }
        private async static Task sendToNstApp(string message)
        {
            try
            {
                ValueSet sentData = new ValueSet();
                sentData.Add("Message", message);
                sentData.Add("Sender", "bkCloudManager");
                sentData.Add("Receiver", "bkNstApplication");

                await IPCservice.SendMessageAsync(sentData);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendToNstApp: " + ex.Message);

                await Logging.WriteSystemLog("sendToNstApp - {0}", ex.Message);
            }
        }

                      

        static private async Task<string> dbGetIpnode(string devName, string devType)
        {
            string connectDB = "Filename=c:\\Database\\Gateway.db";
            string selectStatement;
            if (devType.Equals("CMD"))
            {
                selectStatement = "SELECT CMD_IPV6_Address FROM CMDs WHERE CMD_Unique_ID = " + "\"" + devName + "\"";

            }
            else
            {
                selectStatement = "SELECT CMG_IPV6_Address FROM CMGs WHERE CMG_Unique_ID = " + "\"" + devName + "\"";

            }
            string ipnode = null;
            using (SqliteConnection db = new SqliteConnection(connectDB))
            {
                db.Open();
                using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                {
                    using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                    {
                        try
                        {
                            while (rdr.Read())
                            {
                                ipnode = rdr.GetString(0);
 
                            }
                            return ipnode;
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("dbGetIpnode:" + error.Message);
                            await Logging.WriteDebugLog("dbGetIpnode - {0}", error.Message);
                            db.Close();
                            return null;
                        }
                    }
                }
            }
        }



        static private async Task<int> dbDeleteAppCommand(string token)
        {

            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))

            {
                SqliteCommand deleteCommand;
                //string connectionString = null;
                int result = -1;

                try
                {
                    db.Open();

                    deleteCommand = db.CreateCommand();
                    deleteCommand.CommandText = "DELETE FROM app_commands WHERE token=@token";
                    deleteCommand.Prepare();
                    deleteCommand.Parameters.AddWithValue("@token", token);
                    result = deleteCommand.ExecuteNonQuery();
                    db.Close();
                }
                catch (SqliteException error)
                {
                    //Handle error
                    Debug.WriteLine("dbDeleteAppCommand: " + error.Message);
                    await Logging.WriteDebugLog("dbDeleteAppCommand - {0}",  error.Message);
                    db.Close();
                    return -1;

                }

                db.Close();
                return result;
            }

        }

        static private async Task<string> dbGetGatewayAzureConnectionString()
        {

            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))

            {
                SqliteCommand selectCommand;
                //SqliteCommand cmd;
                SqliteDataReader query;
                string connectionString = null;

                try
                {
                    db.Open();

                    selectCommand = new SqliteCommand("SELECT GW_AzureConnString from Gateways", db);

                    query = selectCommand.ExecuteReader();

                    while (query.Read())
                    {
                        connectionString = query.GetString(0);
                        Debug.WriteLine("Azure Connection String:" + connectionString);
                        await Logging.WriteDebugLog("Azure Connection String: {0}", connectionString);
                    }
                }
                catch (SqliteException error)
                {
                    //Handle error
                    Debug.WriteLine("dbGetGatewayAzureConnectionString: " + error.Message);
                    await Logging.WriteDebugLog("dbGetGatewayAzureConnectionString - {0}", error.Message);
                    db.Close();
                    return null;
                }

                db.Close();
                return connectionString;
            }

        }

        static private async Task<string> dbGetGatewayName()
        {

            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))

            {
                SqliteCommand selectCommand;
                //SqliteCommand cmd;
                SqliteDataReader query;
                string gatewayName = null;

                try
                {
                    db.Open();

                    selectCommand = new SqliteCommand("SELECT GW_Unique_ID from Gateways", db);

                    query = selectCommand.ExecuteReader();

                    while (query.Read())
                    {
                        gatewayName = query.GetString(0);
                    }
                }
                catch (SqliteException error)
                {
                    //Handle error
                    Debug.WriteLine("dbGetGatewayName: " + error.Message);
                    await Logging.WriteDebugLog("dbGetGatewayName - {0}", error.Message);
                    db.Close();
                    return null;
                }

                db.Close();
                return gatewayName;
            }

        }


        static private async Task<string> dbGetAzureMessageID(string token)
        {
            string connectDB = "Filename=c:\\Database\\Gateway.db";
            string selectStatement = "SELECT az_message_id FROM app_commands WHERE token = " + "\"" + token.ToUpper() + "\"";
            string azureID = null;
            using (SqliteConnection db = new SqliteConnection(connectDB))
            {
                db.Open();
                using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                {
                    using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                    {
                        selectCommand.Parameters.AddWithValue("@token", token);
                        try
                        {
                            while (rdr.Read())
                            {
                                azureID = rdr.GetString(0);
                                Debug.WriteLine("Azure Connection String:" + azureID);
                            }
                            return azureID;
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("dbGetAzureMessageID: " + error.Message);
                            await Logging.WriteDebugLog("dbGetAzureMessageID - {0} ", error.Message);
                            db.Close();
                            return null;
                        }
                    }
                }
            }
        }

        private static string clarifyConnectionStatusChangeReason(ConnectionStatusChangeReason reason)
        {
            string strReason = null;
            switch (reason)
            {
                case ConnectionStatusChangeReason.Bad_Credential:
                    strReason = "Bad_Credential";
                    break;
                case ConnectionStatusChangeReason.Client_Close:
                    strReason = "Client_Close";
                    break;
                case ConnectionStatusChangeReason.Communication_Error:
                    strReason = "Communication_Error";
                    break;
                case ConnectionStatusChangeReason.Connection_Ok:
                    strReason = "Connection_Ok";
                    break;
                case ConnectionStatusChangeReason.Device_Disabled:
                    strReason = "Device_Disabled";
                    break;
                case ConnectionStatusChangeReason.Expired_SAS_Token:
                    strReason = "Expired_SAS_Token";
                    break;
                case ConnectionStatusChangeReason.No_Network:
                    strReason = "No_Network";
                    break;
                case ConnectionStatusChangeReason.Retry_Expired:
                    strReason = "Retry_Expired";
                    break;
            }
            return strReason;
        }

    }

}