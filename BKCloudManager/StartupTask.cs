﻿using System;
using Windows.ApplicationModel.Background;
using System.Threading.Tasks;
using Windows.Storage;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Windows.ApplicationModel.AppService;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.System.Threading;
using comNST;
using Microsoft.Azure.Devices.Shared;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace BKCloudManager
{
    public sealed class StartupTask : IBackgroundTask
    {

        private ThreadPoolTimer _timer;
        private BackgroundTaskDeferral _deferral;
        int count = 0;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a BackgroundTaskDeferral and hold it forever if initialization is sucessful.
            _deferral = taskInstance.GetDeferral();

            //AppServiceBridge.RequestReceived += AppServiceRequestHandler;
            await IPCservice.InitAsync();

            taskInstance.Canceled += (IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason) =>
            {
                Debug.WriteLine("Cancelled: reason " + reason);
                _deferral.Complete();
            };

            MemoryManager.AppMemoryUsageIncreased += MemoryManager_AppMemoryUsageIncreased;

            IoTClient.Start();

            // _timer = ThreadPoolTimer.CreatePeriodicTimer(DeleteTwinNstCommandHistory, TimeSpan.FromMinutes(5));
            // DeleteTwinNstCommandHistory(null);
        }

        private async void DeleteTwinNstCommandHistory(ThreadPoolTimer timer)
        {
            try
            {
                Debug.WriteLine("Delete a command from NstCommandHistory");

                Twin twin = await IoTClient.deviceClient.GetTwinAsync();

                Newtonsoft.Json.Linq.JObject d = Newtonsoft.Json.Linq.JObject.FromObject(twin.Properties.Desired["DeleteCommandHistory"]);
                var properties = d.Properties();
                TwinCollection nstCommandHistory = null, reportedProperties = null;
                reportedProperties = new TwinCollection();
                nstCommandHistory = new TwinCollection();
                foreach (var x in properties)
                {
                    //CommandHistory temp = x.Value.ToObject<CommandHistory>();

                    // Get messageId than needs to be deleted
                    string messageId = x.Name;

                    // Delete messageId from reported properties

                    nstCommandHistory[messageId] = null;
                    reportedProperties["NstCommandHistory"] = nstCommandHistory;
                }

                await IoTClient.deviceClient.UpdateReportedPropertiesAsync(reportedProperties);



            }
            catch (Exception ex)
            {
                Debug.WriteLine("while retrieving device twin: ", ex.Message);
            }
            return;
        }

        private void MemoryManager_AppMemoryUsageIncreased(object sender, object e)
        {
            var level = MemoryManager.AppMemoryUsageLevel;
            if (level != AppMemoryUsageLevel.Low)
            {
                Debug.WriteLine($"Memory limit {MemoryManager.AppMemoryUsageLevel} crossed: Current: {MemoryManager.AppMemoryUsage}, limit: {MemoryManager.AppMemoryUsageLimit}");
            }
        }

    }
}

